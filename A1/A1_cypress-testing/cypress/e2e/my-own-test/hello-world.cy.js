/// <reference types="cypress" />
//  const token ='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImVkY2hyaXN0eTk5IiwiX2lkIjoiNjMzMmY4NWY5MzM2ZDEwMDA5NzU0MTE2IiwibmFtZSI6IkVkd2luYSBDaHJpc3R5IiwiaWF0IjoxNjY0Mjg1MzQyLCJleHAiOjE2Njk0NjkzNDJ9.uoo65ya9nUirNKay5J6JnVWvnCflHdl4Y8-R5Q4r-o8'
      
describe ('Basic Test', ()=>{
    // before(() => {
    //     cy.then(() =>{
    //         window.localStorage.setItem('__auth__token', token )
    //     })
    // })

    beforeEach(()=>{
        cy.viewport('macbook-16')
        cy.visit('https://codedamn.com/')
       
    })
    
    it('Correct page title?', () => {

        cy.contains('codedamn').should('exist')
    })

    it('Login page properly works', () => {

        cy.contains('Sign in').click()
        cy.contains('Forgot your password?').should('exist')

        cy.contains('Remember me').should('exist')
    })

    it('the login page links work', () =>{
        cy.contains('Sign in').click()

        cy.log('Going to forgot password')

        cy.contains('Forgot your password?').click({force:true})

        cy.url().should('include','/password-reset')

        // cy.url().then((value) => {
        //     cy.log('The current real URL is:', value)
        // })

        cy.go('back')
        cy.contains('Create one').click()
        cy.url().should('include','/register')

    })
    
    it('login should not works, right error info', () =>{
        cy.contains('Sign in').click()
        cy.contains('Unable to authorize').should('not.exist')
        //type('') for automatically typing
        //cy.get('[data-testid="username"]').should("be.enabled").type('admin')
        //cy.get('[data-testid="password"]').type('admin')
        cy.get('[data-testid="username"]').type('admin', {force:true})
        cy.get('[data-testid="username"]').type('admin', {force:true})
        cy.get('[data-testid="password"]').type('admin', {force:true})
        cy.get('[data-testid="login"]').click({force:true})
        cy.contains('Unable to authorize.').should('exist')
    })

    it.only('Login test should work fine', () => {

        //token for auto login
        cy.contains('Sign in').click()

        cy.get('[data-testid="username"]').type('edwinachristy99@gmail.com', { force: true }) //.type('') is for auto type
        cy.get('[data-testid="username"]').type('edwinachristy99@gmail.com', { force: true }) //.type('') is for auto type
       
        cy.get('[id="password"]').type('#Sopran1Blue', { force: true }) //.type('') is for auto type

        cy.get('[data-testid="login"]').click({ force: true })

        cy.url().should('include', '/dashboard')
    });




    // it.only('Login Should display correct error',() =>{
    //     cy.contains('Sign in').click()
    //     cy.url().should('include','/login')
    //     cy.contains('Unable to authorize').should('not.exist')

    //     cy.get('[data-testid="username"]').type('admin')
    //     cy.get('[data-testid=password]').type('admin')
    //     cy.get('[data-testid=login]').click()

    //     cy.contains('Unable to authorize').should('exist')

    // })

})
